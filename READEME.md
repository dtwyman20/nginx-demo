# nginx-demo

This is a demonstration of having a volume on the host that will allow the introduction of other files that can be used in the container

This docker image starts out with a static page being copied into the /usr/share/nginx/html folder from the source directory (at build time)
The docker image is built:

    docker build -t nginx-demo .

The docker image is then executed:

    docker run --rm --name website -v <folder path on host>:/usr/share/nginx/html:rw -d -p <host port>:80

    Example: docker run --rm --name website -v /Users/dino/static-content:/usr/share/nginx/html:rw -d -p 9000:80

This will perform the following:
 * Execute the container in detached mode (-d)
 * Remove the container at the conclusion of the run (--rm)
 * Gives the container a name, instead of the docker random name (--name)
 * Publishes the container port (80) to the specified port on the host (-p)
 * Maps a folder on the host to a folder in the container (-v). Also sets the container's accesss to the volume to read/write (rw)

Initially the host folder is empty, but once the container is running, you can then insert additional files into the folder that
can be used by the container.

In this example, the index.html file that was copied into the image at build time had links to pages that did not exist. Once the 
continaer was running, I copied two files (static pages that were the target of the links) into the host folder. Without restarting the 
Nginx server, the initial page was able to link to the other pages.

